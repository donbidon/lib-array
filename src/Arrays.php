<?php
/**
 * Arrays related library.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Lib;

/**
 * Arrays related library.
 *
 * @static
 */
class Arrays
{
    /**
     * Key
     *
     * @var      string|int
     * @see      self::searchRowBySubkeyValue()
     * @see      self::filterBySubkey()
     * @internal
     */
    protected static $key;

    /**
     * Value
     *
     * @var      mixed
     * @see      self::searchRowBySubkeyValue()
     * @see      self::filterBySubkey()
     * @internal
     */
    protected static $value;

    /**
     * Flag specifying to compare values strict way
     *
     * @var      bool
     * @see      self::searchRowBySubkeyValue()
     * @see      self::filterBySubkey()
     * @internal
     */
    protected static $strict;

    /**
    * Searches two-dimensional array rows for a given pair key/value and returns
    * the corresponding rows.
    *
    * Example:
    * ```php
    * use \donbidon\Utility\Arrays;
    *
    * $data = [
    *     'first'  => ['id' => 2,  'name' => 'Patricia Peloquin', 'sex' => 'yes',      ],
    *     'second' => ['id' => 12, 'name' => 'Deedee Koerner',    'sex' => 'no',       ],
    *     'third'  => 'not an array, will never be found',
    *     'fourth' => ['id' => 85, 'name' => 'Buford Devereaux',  'sex' => 'male',     ],
    *     'fifth'  => ['id' => 06, 'name' => 'Kaci Hillyard',     'sex' => 'ofcaurse', ],
    * ];
    *
    * $result = Arrays::searchRowBySubkeyValue($data, 'id', '2');
    * print_r($result);
    * ```
    * will output:
    * ```
    * Array
    * (
    *     [first] => Array
    *         (
    *             [id] => 2
    *             [name] => Patricia Peloquin
    *             [sex] => yes
    *         )
    * )
    * ```
    * ```php
    * $result = Arrays::searchRowBySubkeyValue($data, 'id', '2', FALSE);
    * print_r($result);
    * ```
    * will output:
    * ```
    * Array
    * (
    *     [0] => Array
    *         (
    *             [id] => 2
    *             [name] => Patricia Peloquin
    *             [sex] => yes
    *         )
    * )
    * ```
    * ```code
    * $result = Arrays::searchRowBySubkeyValue($data, 'id', '2', TRUE, TRUE);
    * print_r($result);
    * ```
    * will output:
    * ```
    * Array
    * (
    * )
    * ```
    * ```php
    * $result = Arrays::searchRowBySubkeyValue($data, 'name', 'Kaci Hillyard');
    * print_r($result);
    * ```
    * will output:
    * ```
    * Array
    * (
    *     [fifth] => Array
    *         (
    *             [id] => 6
    *             [name] => Kaci Hillyard
    *             [sex] => ofcaurse
    *         )
    * )
    * ```
    * ```php
    * $result = Arrays::searchRowBySubkeyValue($data, 'name', '/ER/i');
    * print_r($result);
    * ```
    * will output:
    * ```
    * Array
    * (
    *     [second] => Array
    *         (
    *             [id] => 12
    *             [name] => Deedee Koerner
    *             [sex] => no
    *         )
    *     [fourth] => Array
    *         (
    *             [id] => 85
    *             [name] => Buford Devereaux
    *             [sex] => male
    *         )
    * }
    * ```
    *
    * @param  array      $haystack      The array
    * @param  int|string $key           The searched key
    * @param  mixed      $value         The searched value, if passed as string
    *                                   and starts from '/' symbol, will be
    *                                   processed as regular expression, in this
    *                                   case $strict argument will be ignored
    * @param  bool       $preserveKeys  Flag specifying to maintain rows index
    *                                   assotiation
    * @param  bool       $strict        Flag specifying to compare values strict
    *                                   way
    * @return array
    * @todo   Implement regexp for key
    */
    public static function searchRowBySubkeyValue(
        array $haystack, $key, $value, $preserveKeys = TRUE, $strict = FALSE
    )
    {
        self::$key    = $key;
        self::$value  = $value;
        self::$strict = (bool)$strict;

        $result = array_filter($haystack, array('self', 'filterBySubkey'));
        if (!$preserveKeys && sizeof($result)) {
            $result = array_combine(
                range(0, sizeof($result) - 1),
                array_values($result)
            );
        }
        self::$key   = NULL;
        self::$value = NULL;

        return $result;
    }

    /**
     * Sort two-dimensional array by column preserving row keys.
     *
     * @param  array      $array     <b>[by ref]</b> Array
     * @param  int|string $column    Sort column
     * @param  int        $sort      Sort type
     * @param  int        $direction Sort direction: SORT_ASC or SORT_DESC
     * @return void
     * @link   http://php.net/manual/en/function.array-multisort.php
     *         PHP documentation for available sort types
     */
    public static function sortByCol(
        array &$array, $column, $sort = SORT_STRING, $direction = SORT_ASC
    )
    {
        if (!sizeof($array)) {
            return;
        }

        $index = [];
        $i = 0;
        foreach ($array as $key => $row) {
            if (is_array($row)) {
                $index['pos'][$i]  = $key;
                $index['name'][$i] = $row[$column];
                ++$i;
            }
        }
        array_multisort($index['name'], $sort, $direction, $index['pos']);
        $result = array();
        for ($j = 0; $j < $i; ++$j) {
            $result[$index['pos'][$j]] = $array[$index['pos'][$j]];
        }
        $array = $result;
    }

    /**
     * Adapt column containig sort order as integer values for using array_multisort().
     *
     * Let we want to sort one array using other array as order:
     * ```php
     * $order = [2, 1];
     * $data  = ['aaa', 'bbb'];
     * array_multisort($order, SORT_NUMERIC, SORT_ASC, $data);
     * print_r($data);
     * ```
     * will output:
     * ```
     * Array
     * (
     *     [0] => bbb
     *     [1] => aaa
     * )
     * ```
     * But
     * ```php
     * $order = [1, 1];
     * $data  = ['bbb', 'aaa'];
     * array_multisort($order, SORT_NUMERIC, SORT_ASC, $data);
     * print_r($data);
     * ```
     * will change order of data and output:
     * ```
     * Array
     * (
     *     [0] => aaa
     *     [1] => bbb
     * )
     * ```
     * Using this class method you can prevent changing order of data.
     *
     * @param  array $column  <b>[by ref]</b> Array containing integer values as order for data
     * @return void
     * @see    http://php.net/manual/en/function.array-multisort.php
     */
    public static function adaptOrderCol(array &$column)
    {
        $minOrder = min($column);
        do {
            $counts = array_count_values($column);
            ksort($counts);
            $loop = FALSE;
            foreach ($counts as $order => $count) {
                if ($count < 2) {
                    continue;
                }
                $current = array_search($order, $column);
                if ($order > $minOrder) {
                    $minOrder = $order;
                }
                foreach (array_keys($column) as $key) {
                    if ($column[$key] >= $minOrder && $key != $current) {
                        ++$column[$key];
                    }
                }
                $minOrder = $order + 1;
                $loop = TRUE;
                break; // foreach ($counts as $order => $count)
            }
        } while ($loop);
    }

    /**
     * Filters two-dimensional array for a given pair key/value.
     *
     * @param    mixed $row
     * @return   bool
     * @see      self::searchRowBySubkeyValue()
     * @internal
     */
    protected static function filterBySubkey($row)
    {
        $result = FALSE;
        if (is_array($row) && array_key_exists(self::$key, $row)) {
            if ('/' == substr(self::$value, 0, 1)) {
                $result = preg_match(self::$value, $row[self::$key]);
            } else {
                $result = self::$strict
                    ? self::$value === $row[self::$key]
                    : self::$value == $row[self::$key];
            }
        }

        return $result;
    }
}
