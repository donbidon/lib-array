# Arrays related library

### Installing
---
Add following code to your "composer.json" file
```json
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/donbidon/lib-array"
        }
    ],
    "require": {
        "donbidon/lib-array": "dev-master"
    }
```
and run `composer update`.
